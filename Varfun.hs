double x = 2 * x
quadruple x = double (double x)
square x = x * x
half x = x / 2
substract x = (half x) - 12
areaRect l w = l * w
areaTriangle b h = (b * h) / 2
minus x y = x - y
volumen w l h = w * l * h
volumenPiramide b h = b * b * h / 3
areaCirc r = pi*r^2
volumenCilindro b h = (areaCirc (b/2)) * h

{-Funciones locales con la fórmula de Heron-}
areaTriangleTrig a b c = c * height / 2 -- usamos trigonometría
    where
      cosa = (b^2 + c^2 - a^2) / (2 * b * c)
      sina = sqrt (1 - cosa^2)
      height = b * sina
areaTriangleHeron a b c = result -- usamos la fórmula de Herón
    where
      result = sqrt (s * (s-a) * (s-b) + (s-c))
      s = (a + b + c) / 2

absolute x
    | x < 0 = -x
    | otherwise = x

numOfRealSolutions a b c
    | disc > 0 = 2
    | disc == 0 = 1
    | otherwise = 0
        where
        disc = b^2 - 4*a*c

cabezaYCola lista = (head lista, tail lista)

quintoElemento lista = head (tail (tail (tail (tail lista))))

elementoCuatro lista =  snd(fst(("Hola",4),True))
