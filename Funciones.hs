-- | Función toma.
--   Recibe un número n y una lista l.
--   Devuelve la lista que contiene los primeros n elementos de l.

toma :: Int -> [a] -> [a]
toma n _
    | n <= 0 = []
toma _ [] = []
toma n (x:xs) = x : toma (n-1) xs

-- | Función quita
--   Recibe un número n y una lista l.
--   Devuelve la lista que quita los primeros n elementos de l.

quita :: Int -> [a] -> [a]
quita n xs
    | ((n <= 0) || null xs) = xs
    | otherwise = quita (n-1) (tail xs)

-- | Función elemento.
--   Definida formalmente como:
--   elemento x l si y sólo si x es elemento de la lista l.
elemento :: Int -> [Int] -> Bool
elemento x [] = False
elemento x (y:ys)
 | x == y = True
 | otherwise = elemento x (ys)

-- | Función replica.
--   Toma un número n y un elemento "a" como entrada.
--   Devuelve la lista que contiene n veces a "a" como elemento.

replica :: Int -> a -> [a]
replica n x
    | n <= 0    = []
    | otherwise = x:replica (n-1) x

-- | Función ultimo.
--   Devuelve el último elemento de una lista no vacía.
ultimo :: [a] -> a
ultimo [] = error "No se permiten listas vacías."
ultimo [a] = a
ultimo (x:xs) = ultimo(xs)

-- | Función eultimo.
--   Devuelve la lista resultante al borrar el último elemento de una lista
--   no vacía.

eultimo :: [a] -> [a]
eultimo [] = error "No se permiten listas vacías."
eultimo [a] = []
eultimo (x:xs) = x:eultimo(xs)
