# Apuntes de Haskell

`:load`   - cargar archivos .hs

`:reload` - recargar archivos

`:quit`   - salir

`:cd`     - cambiar de directorio

# Comentarios

Comentario simple: `--`

Multilínea: `{-          -}`

# Funciones
Declaramos expresiones poniendo el argumento antes del igual (=).

¿Cómo GHCi evalua quadruple 5?

`quadruple x = double (double x)`

Evaluación:
          
     -> quadruple 5 = double (double 5)
   
     -> = double (10)

     -> = 20
Define una función que reste 12 de la mitad de su argumento.

     substract x = (half x) - 12

     half x = x /2

# Multiparámetros
Las funciones pueden tomar más de un argumento.

Los argumentos múltiples deben separarse por espacios. En algunas ocasiones 
tenemos que usar los parentésis para agrupar expresiones.

El siguiete código lanza error:

`quadruple x = double double x      -- error`

Esto sucede porque aplicaría una función llamada `double` entre los dos 
argumentos `double` y `x`. Notaremos (en un futuro) que las funciones
pueden ser argumentos de otras funciones.

Los argumentos deben de ser siempre pasados en el orden dado.

     minus x y = x - y
     *Main> minus 10 5
     5
     *Main> minus 5 10
     -5
    
Escribe una función que calcule el volumen de una caja.

`Volumen : width * length * height`

Aproximadamente cuántas piedras tiene la famosa piramide de Giza.
(Hint: Estimar el volumen de la piramide y el volumen de cada bloque)

     volumenPiedra w l h = w * l * h
     volumenPiramide b h = b * b * b / 3
     *Main> volumenPiramide 230.4 146.5 / volumen 1 1 1
     2592276.48

# Combinando funciones

Podemos usar funciones que ya definimos como nuevas funciones.

Esto se puede ver para calcular el área de un rectángulo y un cuadrado.

    areaRect l w = l * w
    areaSquare s = areaRect s s
    
    *Main> areaSquare 5
    25

Escribe una función que calcule el volumen de un cilindro. El volumen de un
cilindro es el area de la base (la cual es un círculo) multiplicada por su 
altura.

    volumenCilindro b h = (areaCirc(b/2) * h)

# Definiciones locales
`where` clauses

Cuando definimos una función, podríamos querer resultados intermedios, lo que
se llaman funciones locales.

Hay que notar que `where` y las definiciones locales están identadas por 
4 espacios.

# Scope
La noción de llamar a alguien dependiendo el contexto en algunos lenguajes de
programación se les llama scope.

Ya que cuando hacemos lo siguiente:
    
    Prelude> let r = 0
    Prelude> let area r = pi*r^2
    Prelude> area 5
    78.53981533974483
En este contexto estamos hablando de otra `r`, es por eso que Haskell no nos
retorna un 0 como evaluación.

# Resumen Parte I
 + Las variables almacenan valores (que pueden ser cualquier expresión 
   arbitraria).
 + Las variables no cambian dentro de un ámbito.
 + Las funciones nos ayudan a escribir código reutilizable. 
 + Las funciones pueden aceptar más de un parámetro.

# Igualdad y otras comparaciones
Como Haskell usa el simbolo `=` para asignaciones, se opta por usar el simbolo
`==` para igualdades. 
    
    Prelude> 2 + 3 == 5
    True
    Prelude> 7 + 3 == 5
    False
    
También podemos usarlo con funciones

    Prelude> let f x = x + 3
    Prelude> f 2 == 5
    True
Esto funciona porque `f 2` evalua a `2 + 3`.

Podemos comparar también valores númericos con `<`, `>`, `>=`, `<=`

# Valores booleanos
Sólo existen 2 y son `True` o `False`. Estos valores representan la 
veracidad de una proposición.

# Introduccion a tipos
`True` y `False` son valores reales, no una simple analogía. Los valores 
booleanos tienen el mismo estatus como los valores numericos en Haskell.

Tendremos errores si tratamos de comparar un número con un NO-número o un
booleano con un NO-booleano.

# Operadores infijos
Haskell permite que las funciones de dos argumentos se escriban como operadores
infijos colocados entre sus argumentos. 

Si queremos escribir una función de la fomra estándar (escribiendo primero el 
nombre de la función antes de los argumentos como un operador prefijo) la 
función deberá ser encerrada en parentesis.

    Prelude> 4 + 9 == 13
    True
    Prelude> (==) (4+9) 13
    True

# Operadores booleanos
Haskell provee tres basicas funciones para poder manipular la verdad de los 
valores en la lógica propoisicional:

    (&&) Operador "and"
    (||) Operador "or"
    (not) Operador "not", negación de un valor booleano.
    
La librería de Haskell incluye un operador relacional (`/=`) para el no igual a,
pero se puede implementar como:

    x /= y = not (x == y)

# Guardas
En Haskell es usual usar los operadores booleanos de forma conveniente y con una
abreviación en la sintaxis. A esto usualmente se le llama azúcar sintactica.
Ya que es una forma "fancy" de código para nuestra perspectiva.

Cada una de las guardas empieza con un carácter de tubo `|`. Después colocamos
expresión a la cual evaluaremos a un booleano o también llamado predicado.

La expresión `otherwise` es el caso cuando ninguna de los anteriores predicados
es evaluado a verdadero.

Siempre es bueno proveer un `otherwise` como guarda final, ya que pueden ocurrir
ciertos errores en tiempo de ejecución.

# `where` y guardas
La clausula `where` funciona también con las guardas. El ejemplo más fácil de 
ver es con una función que encuentre las soluciones reales de una
ecuación cuadratica.

    numOfRealSolutions a b c
    | disc > 0 = 2
    | disc == 0 = 1
    | otherwise = 0
        where
        disc = b^2 - 4*a*c

# Tipos básicos

En Haskell usamos los tipos para reducir el número de errores en el código
Los tipos son usados para agrupar valores similares en una categoria.

Por convención en Haskell escribimos el nombre del tipo empezando por una
letra mayúscula.

# Usando el comando interactivo `:type`
En GHCi podemos usar el comando `:type` o `:t` para ver de qué tipo es una 
función o variable.

    Prelude> :t True
    True :: Bool
    Prelude> :t (3 < 5)
    (3 < 5) :: Bool
    Prelude> :t half
    half :: Fractional a => a -> a
    
El simbolo `::` que aparece se puede leer como "es del tipo" e indica la firma
del tipo.

Los valores booleanos no sólo son para validar comparaciones. `Bool` captura la
semantica del si/no en una respuesta. Puede representar cualquier información de
ese estilo. Un ejemplo es cuando se prende o apaga un botón.

# Caracteres y Cadenas
Podemos probar `:t` de la siguiente forma:

    Prelude> :t 'H'
    'H' :: Char
Con esto vemos que 'H' es del tipo `Char`. Las comillas simples `('')` sirven 
sólo para carácteres individuales, si necesitamos un texto más largo debemos 
usar comillas dobles `("")`

Haskell interpreta las cadenas como listas de carácteres, es por eso que la 
salida es la siguiente:

    Prelude> :t "Hola Mundo"
    "Hola Mundo" :: [Char]

Trata de usar `:type` en el valor `"H"` (con doble comillas). ¿Qué pasa? 
¿Por qué?

    Prelude> :t "H"
    "H" :: [Char]
Recordemos que para Haskell que algo este en dobles comillas será interpretado 
como una lista de carácteres y no como un carácter simple.

Trata de usar `:type` en el valor `'Hola Mundo'` (comillas simples). ¿Qué pasa?
¿Por qué?

    Prelude> :t 'Hola Mundo'
    <interactive>:1:1: error:
    • Syntax error on 'Hola
      Perhaps you intended to use TemplateHaskell or TemplateHaskellQuotes
    • In the Template Haskell quotation 'Hola
    
Nos lanza un error ya que con las comillas simples no estamos trabajando con una
lista de carácteres, sino con carácter simple. Por lo que Haskell no puede 
interpretarlo correctamente.

Haskell tiene algo llamado tipos sinonimos, lo cual ayuda a usar 
alternativamente otro nombre para los tipos. Por ejemplo, `String` es definido
como un sinónimo de `[Char]`, así que podemos sustituirlo.

    Prelude> let holaMundo = "Hola Mundo" :: String
    Prelude> :t holaMundo
    hola :: String
    
# Tipos funcionales
Lo que hace a Haskell un sistema de tipos realmente poderoso es que: 
Las funciones pueden tener tipos también.

Ejemplo: `not`

Sabemos que `not` niega un valor booleano. Para poder ver el tipo de una función
debemos considerar dos cosas: 
+ El tipo de valor que toma como entrada
+ El tipo de valor que regresa como salida.


    not :: Bool -> Bool
    
Aqui `not` toma un `Bool` (el que será negado) y devuelve un `Bool` (el negado).

Ejemplo: `chr` y `ord`

Podemos conocer el número que le corresponde a un carácter con `ord` y con 
`chr` saber que vlaor le corresponde a un carácter.
Los tipos de `chr` y `ord` son:

    chr :: Int -> Char
    ord :: Char -> Int
Para poder leer este tipo de funciones tenemos que cargar el módulo `Data.Char`
esto se hace haciendo: `:module Data.Char` o `:m Data.Char`

    Prelude> :m Data.Char
    Prelude Data.Char> chr 97
    'a'
    Prelude Data.Char> ord 'c'
    99

# Funciones con más de un argumento

En Haskell nos gustaría escribir funciones con más de un argumento

Función con más de un argumento:

    xor p q = (p || q) && not (p && q)

Para poder escribir una función que acepte más de un argumento debemos escribir
en orden todos los tipos de argumentos y conectarlos conn un `->`. Finalmente
añadir un tipo para el resultado al final con un `->`.

En el caso de `xor` la firma sería:

    xor :: Bool -> Bool -> Bool
   
Una biblioteca es una colección de código usado en varios programas.

Incluso si no hemos visto nunca una función o no sabemos exactamente cómo 
funciona, su firma nos debería ser más que suficiente para poder saber de
manera general qué es lo qué hace.

Un gran habito es testear cada nueva función que conocemos con `:t`.

`negate` función es: `negate :: Num a => a -> a`

`(||)` función es: `(||) :: Bool -> Bool -> Bool`

`monthLength` función es: `monthLength :: Bool -> Int -> Int`

`f x y = not x && y` función es: `f :: Bool -> Bool -> Bool`

`g x = (2*x - 1)^2` función es: `g :: Int -> Int`

# Asignación de tipos en código
En Haskell la asignación de tipos va arriba de la definición correspondiente de
la función

    xor :: Bool -> Bool -> Bool
    xor p q = (p || q) && not (p && q)
    
La asignación de esta forma tiene dos roles: Aclarar el tipo de la función para
quien lee el código y para el compilador/interprete

# Inferencia de tipos
Cuando uno no le dice a Haskell de que tipo son las funciones de sus variables,
tiene que inferirlo. En esencia, el compilador empieza con el tipo de cosas que
sabe y calcula los tipos del resto de los valores.

    isL c = c == 'l'
    
En ese código lo que sucede es que el interprete sabe que `'l'` es un Char.
Y ve que estamos haciendo una comparación booleana con `c == 'l'`, entonces
tiene que devolver un `Bool`, por lo que la firma del método es:
    
    isL :: Char -> Bool

Podemos verificarlo haciendo `:t isL` en `GHCi`

La pregunta que puede surgir aquí es que ¿para qué hacemos la asignación de
tipos si Haskell puede inferirlos?

Porque en algunas ocasiones el interprete carece de información para inferir el 
tipo por lo que la firma se vuelve obligatoria. En algunos otros casos, podemos
usar una firma de tipo para influir hasta cierto punto en el tipo final de una 
función o valor.

Otras razones para asignar tipos son:
+ Documentación
+ Debugging

Cuando las funciones tienen el mismo tipo tenemos la opción de escribir una
simple asignación para todas ellas, separandolas por una coma.
Como en el caso de `uppercase` y `lowercase`

    module StringManip where
    import Data.Char
    
    uppercase, lowercase :: String -> String
    uppercase = map toUpper
    lowercase = map toLower
    
Tipar previene errores. Cuando pasamos una expresión debemos estar seguro que 
los tipos sean iguales, sino tendremos un type erros cuando tratemos de 
compilarlo, el programa no pasará el typecheck.

    Ejemplo: Un programa que no coincide el tipado
    "hola" + " mundo"       -- type error
    
Esta línea no es correcta ya que no podemos unir dos Strings de esta forma
para hacerlo correctamente necesitamos que el operador de concatenación sea el
correcto.

    Ejemplo: El error anterior se arregla
    "hola" ++ " mundo"      -- "hola mundo"
    
# Listas y tuplas
Haskell usa dos estructuras fundamentales para administrar muchos valores: 
listas y tuplas. Ambas trabajan agrupando multiples valores en un sólo valor
combinado.

    Prelude> let numeros = [1,2,3,4]
    Prelude> let booleanos = [True, False, False]
    Prelude> let cadenas = ["Aquí", "hay", "cadenas"]

Los corchetes delimitan listas y los elementos individuales son separados por
una coma.
La única restricción importante es que los elementos en una lista deben de ser
del mismo tipo.

# Contruyendo listas
Podemos contruir listas con `[]` y las comas, pero también podríamos contruirlas
parte por parte usando el operador `(:)` llamado "cons".
Este operador viene de la terminología de LISP que invetaron el verbo "to cons",
una mnemotecnia para constructor.

    Ejemplo: Contruyendo algo en una lista.
    Prelude> let numeros = [1,2,3,4]
    Prelude> numeros
    [1,2,3,4]
    Prelude> 0:numeros
    [0,1,2,3,4]
    
Cuando usamos cons en una lista `(algo:unaLista)` obtenemos otra lista de 
vuelta, el operador cons evalua de derecha a izquierda:

    Prelude> 1:0:numeros
    [1,0,1,2,3,4]
    Prelude> 2:1:0:numeros
    [2,1,0,1,2,3,4]
    
Haskell construye las listas de manera que concatena los elementos en una
lista vacía, `[]`. Las comas son azúcar sintactica. Así que podríamos ver que
`[1,2,3,4,5]` es equivalente a `1:2:3:4:5:[]`Los corchetes delimitan listas y los elementos individuales son separados por
una coma.
La única restricción importante es que los elementos en una lista deben de ser
del mismo tipo.

# Contruyendo listas
Podemos contruir listas con `[]` y las comas, pero también podríamos contruirlas
parte por parte usando el operador `(:)` llamado "cons".
Este operador viene de la terminología de LISP que invetaron el verbo "to cons",
una mnemotecnia para constructor.

    Ejemplo: Contruyendo listas
    Prelude> 1:0:numbers
    [1,0,1,2,3,4]
    Prelude> 2:1:0:numbers
    [2,1,0,1,2,3,4]

Haskell construye listas concatenando elementos a la lista vacía `[]`. Las comas
son azúcar sintactica. Así que `[1,2,3,4,5]` es equivalente a `1:2:3:4:5:[]`

Ejercicios:
¿Funciona el siguiente pedazo de código en Haskell `3:[True, False]`? 
¿Por qué o por qué no?

No funciona ya que `3` es un Entero y la lista a la que lo pretendemos agregar 
es una lista de valores booleanos, lo cual nos mandaría un error.
Recordemos que las listas sólo agregan elementos de su mismo tipo.

Escribe una función `cons8` que tome una lista como argumento y agregue 8 al
inicio de esta. Pruebala haciendo las siguientes concatenaciones.

    cons8 []
    cons8 [1,2,3]
    cons8 [True, False]
    let foo = cons8 [1,2,3]
    cons8 foo
    
Solución:
    
    let cons8 lista = 8:lista
    cons8 [] -- [8]
    cons8 [1,2,3] -- [8,1,2,3]
    cons8 [True, False] -- type error
    let foo = cons8 [1,2,3] 
    cons8 foo -- [8,8,1,2,3]
    
Adapta la función de arriba para que ahora 8 se agregue al final de la lista.

Solución:

    let cons8 lista = lista++[8]
    Prelude> cons8 [1,2,3]
    [1,2,3,8]

Escribe una función que tome dos argumentos, una lista y una cosa y concatene
la cosa dentro de la lista:

Solución:

    let myCons lista cosa = cosa:lista
    
# Las cadenas son listas
Sabemos que en Haskell los strings son listas de carácteres.
Entonces podemos manipularlas como cualquier lista.

    Prelude> "hey" == ['h', 'e', 'y']
    True
    Prelude> "hey" == 'h':'e':'y':[]
    True
    
# Listas de listas
Las listas pueden contener lo que sea, siempre y cuando sean del mismo tipo.
Entonces con esto podemos ver que las listas pueden contener otras listas

    Ejemplo: Listas que contienen listas
    Prelude> let listaDeListas = [[1,2],[3,4],[5,6]]
    Prelude> listaDeListas
    [[1,2],[3,4],[5,6]]

Las listas de listas pueden ser algo curiosas ya que en ocasiones las cosas 
dentro de ellas pueden no ser del mismo tipo que las contiene.
El tipo `Int` es diferente al `[Int]`

Ejercicios:

¿Cuál de estas listas son validas en Haskell y cuáles no?

    [1,2,3,[]]
    [1,[2,3],4]
    [[1,2,3],[]]

Solución:

    [1,2,3,[]] -- Inválida, tratamos de concatenar una lista vacía con Int
    [1,[2,3],4] -- Inválida, tratamos de concatener un Int con un [Int]
    [[1,2,3],[]] -- Válida, está lista contiene [[Int],[]]

¿Cuál de estas son validas en Haskell?
    
    []:[[1,2,3],[4,5,6]]
    []:[]
    []:[]:[]
    [1]:[]:[]
    ["hola"]:[1]:[]
    
Solución:

    []:[[1,2,3],[4,5,6]] -- Válida: [[],[1,2,3],[4,5,6]]
    []:[] -- Válida: [[]]
    []:[]:[] -- Válida: [[],[]]
    [1]:[]:[] -- Válida: [[1],[]]
    ["hola"]:[1]:[] -- Inválida: Tratamos de concatenear [String] con [Int]

¿Puede Haskell tener listas de listas de listas?

Solución:

Claro, siempre y cuando sean del mismo tipo.

    Prelude> listaString = [[["Hola"], ["Mundo"]], [["Cruel"]]]
    [[["Hola"], ["Mundo"]], [["Cruel"]]]
    
¿Por qué lo siguiente es una lista inválida en Haskell?

    [[1,2],3,[4,5]]

Solución:

Porque esa lista no es del mismo tipo podemos verlo de la siguiente manera:

    [[Int],Int,[Int]]

Claramente queremos una lista de `[Int]`, pero no podemos meter un `Int` en ella
ya que no es del mismo tipo.

    [[1,2],[3],[4,5]] -- Esto es válido (Una lista de [Int])
    
# Tuplas
Las tuplas son otra manera de poder guardar multiples valores.
La diferencia más sustancial de las Listas y de las Tuplas es que las tuplas 
tienen un número fijo de elementos, esto quiere decir que no podemos concatenar
elementos a una tupla.

También los elementos de una tupla no necesitan ser del mismo tipo, esto puede
servir cuando hacemos por ejemplo una agenda en dónde necesitamos el nombre, 
un número de telefono y cuántas veces hemos marcado a ese número.

Las tuplas van con parentesis `()` y sus elementos van separados por comas

    Ejemplo: Algunas tuplas
    (True, 1)
    ("Hola mundo", False)
    (4,5, "Six", True, 'b')

Las tuplas podrían servir para regresar más de un resultado en una función.

# Tuplas con tuplas (y otras combinaciones)

Como las tuplas también son cosas, podemos guardar listas de listas en ellas o
podemos tener listas de tuplas, etc.

    Ejemplo: Anidamiento de tuplas y listas
    ((2,3), True)
    ((2,3),[2,3])
    [(1,2),(3,4),(5,6)]
    
El tipo de las tuplas no sólo es definido por su tamaño, sino al igual que las
listas por el tipo que las contiene.
La tupla `("Hola",32)` y `(47,"Mundo")"` son fundamentalmente diferentes. Una es
una tupla del tipo `(String, Int)` y la otra `(Int, String)`.

Ejercicios:

¿Cuál de estos son válidos en Haskell y por qué?

    1:(2,3) -- Inválido: No podemos añadir elementos a las tuplas, son fijas.
    (2,4):(2,3) -- Inválido: Mismo razonamiento que arriba.
    (2,4):[] -- Válido: En las listas si podemos agregar elementos. [(2,4)]
    [(2,4),(5,5),('a','b')] -- Inválido: Estamos con una lista de [(Int,Int)],
                               no podemos meter un [(Char,Char)].
    ([2,4],[2,2]) -- Válido: Es una tupla de dos listas y esas listas tienen dos
                     elementos cada una del tipo Int.

# Recuperando valores
Para que las listas y los valores sean útiles, necesitamos tener acceso a los 
valores que contienen.

Con las 2-tuplas podríamos querer obtener las coordenadas de un punto (x,y),
necesitariamos entonces una función que nos obtenga el primero y segundo 
elemento.

    Ejemplo usando fst y snd
    Prelude> fst (2,5)
    2
    Prelude> snd(2,5)
    5
    Prelude> fst (True, "boo")
    True
    Prelude snd (5,"Hola")
    "Hola"
    
Hay que notar que estas funciones sólo trabajan con pares entonces lo siguiente
es inválido:

    Prelude> fst (2,3,4)
    ERROR -- No mostraremos el error completo
    Prelude> snd ("Hola",3,"Mundo")
    ERROR
    
Para las listas, existen las funciones `head` y `tail`, son aproximadamente
análogas a `fst` y `snd`. Lo que haces es desamblar la lista tomando `(:)` que
las une.
`head` evalua el primer elemento de la lista mientras que `tail` nos da el resto

Existe un problema con `head` y `tail` ya que no podemos usarla en la lista
vacía. Lo siguiente es incorrecto:

    Prelude> head[]
    *** Exception: Prelude.head: empty list
    Prelude> tail[]
    *** Exception: Prelude.tail: empty list
    
Esto se puede inferir si vemos los tipos de cada función:
    
    Prelude> :t head
    head :: [a] -> a
    Prelude> :t tail
    tail :: [a] -> [a]

Ejercicios:

Usa una combinación de `fst` y `snd` para extraer el 4 de la tupla
`(("Hola",4),True)`

Solución:

    Prelude> let tupla = (("Hola", 4), True)
    Prelude> snd(fst(tupla))
    4

Escribe una función que regrese la cabeza y la cola de una lista como primer y
segundo elemento de una tupla

    cabezaYCola lista = (head lista, tail lista)

Usa `head` y `tail` para escrbir una función que te de el 5to elemento de una 
lista. 

    quintoElemento lista = head(tail(tail(tail(tail lista))))
    
El inconveniento de definir esto es que nuestro programa no acepta listas 
que sean menores a 5 elementos. Podríamos generalizar esto en algún 
momento.

# Tipos poliformos
En ocasiones vamos a necesitar separar las funciones en cada caso que
presentamos.

    headInt :: [Int] -> Int
    headBool :: [Bool] -> Bool
    headString :: [String] -> String

Pero si hacemos esto anterior sería bastante molesto saber de que tipo sería
nuestra lista, es por eso que tenemos una simple función `head` para cualquier
tipo de lista.

    head :: [a] -> a

De esta forma obtendríamos lo siguiente:
    
    Prelude> head [True, False]
    True
    Prelude> head ["hey", "hola"]
    "hey"
    
La asignatura `a` parece ser un no-tipo. Recordemos que los nombres de los tipos
siempre empiezan con una letra mayúscula.
Pero `a` es una variable de tipo, cuando Haskell ve una variable de tipo, 
permite que cualquier tipo tome su lugar, a esto se le llama polimorfismo.

En este caso la función `head` nos dice que está tomando un  lista `[a]` que 
puede ser de un tipo arbitrario (`a`) y devuelve el mismo valor del mismo tipo.

    f :: a -> a
    f :: a -> b
    
El primero dice que `f` toma un argumento de cualqueir tipo y devuelve algo del
mismo tipo como resultado.

El segundo quiere decir que `f` toma un argumento de cualquier tipo y devuelve
un tipo que puede o no ser el mismo que el primero.

Ejercicios:

Da el tipo de las siguientes funciones:

1. La solución del tercer ejercicio en la sección previa ("... una función que
regrese la cabeza y la cola de las listas como primer y segundo elemento de una
tupla")
2. La solución del cuarto ejercicio de la sección previa ("... una función que
regrese el quinto elemento de una lista").

3. `h x y z = chr (x - 2)` (recuerda que discutimos que es chr en el capítulo 
anterior).

Solución:

1. `cabezaYCola :: [a] -> (a, [a])`

2. `quintoElemento :: [a] -> a`

3. `h x y z :: Int -> a -> b -> Char -- como y, z no son utilizadas, pueden ser
                                        de cualquier tipo.`
                                        

# Resumen
Vimos listas y tuplas, las diferencias entre estás son las siguientes:
1. Las listas son definidas en corchetes y con comas: `[1,2,3]`
   + Las listas pueden contener cualquier tipo, siempre y cuando todos sus
     elementos sean del mismo.
   + Las listas pueden construirse con el operador `(:)`, pero sólo podemos
     concatenar cosas en las listas.
2. Las tuplas se construyen con parentésis y comas: `("Bob",32)`
   + Las tuplas pueden contener cualquier cosa y pueden ser de diferentes tipos
   + La longitud de una tupla se da por su tipo, las tuplas con diferentes 
     longitudes tendrán diferentes tipos. 
   + En las tuplas los elementos son fijos, no podemos aumentarlas.
3. Listas y tuplas pueden ser combinadas en maneras diferentes, llistas con 
   listas, tuplas con listas, etc.

# Tipos básicos II


